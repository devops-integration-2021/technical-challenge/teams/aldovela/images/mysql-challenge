FROM mysql:5.6

MAINTAINER aldovela <aldovela@gmail.com>

COPY mysql-challenge.sql /docker-entrypoint-initdb.d
